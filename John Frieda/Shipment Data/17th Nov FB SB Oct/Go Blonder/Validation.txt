Job Name:                                        FB SB Oct unshipped orders 3448

Software & Version Number:                       iAddress Version 2014

Customer Name:                                   Samplits
Customer Address:                                5595 Finch Ave E
                                                 Toronto ON  M1B2T9
Customer Telephone Number:                       (416) 572-5222
Customer Number:                                 1018221
Mailed on Behalf Of:                             Samplits
Customer Number:                                 1018221
Date of Mailing Selected:                        2017-11-22

Expiry Date of Delivery Mode Data CD:            2017-12-14
Serial Number of Delivery Mode Data CD:          P3102025561101

Options:                           Upper Case
                                   Two Line
                                   Western Style
                                   Shorten Streets
                                   Update Valid
                                   Place Space in Postal Code (OM)
                                   Minimum SERP Rural Standards

Language Options:                  Automatic

Valid:                                              3446
Corrected Addresses:                                   2
Uncorrectable Addresses:                               0
Foreign Addresses:                                     0

LVR Questionable:                                      2
Rural Questionable and Valid:                         50
Rural Questionable and Corrected:                      0



Questionable Rural Addresses:                       1.5%


Percent Valid Before Correction:                   99.9%
Percent Valid:                                    100.0%


Total Records:                                      3448

Date file processed:                             2017-11-17 15:35
Address Accuracy Expiry Date:                    2018-11-17
