Job Name:                                        FB- LV Dec and Prog Ideaon4 side  Ad combined 8249

Software & Version Number:                       iAddress Version 2014

Customer Name:                                   Samplits
Customer Address:                                5595 Finch Ave E
                                                 Toronto ON  M1B2T9
Customer Telephone Number:                       (416) 572-5222
Customer Number:                                 1018221
Mailed on Behalf Of:                             Samplits Inc
Customer Number:                                 1018221
Date of Mailing Selected:                        2017-12-22

Expiry Date of Delivery Mode Data CD:            2018-01-11
Serial Number of Delivery Mode Data CD:          P3102025561101

Options:                           Upper Case
                                   Two Line
                                   Western Style
                                   Shorten Streets
                                   Update Valid
                                   Place Space in Postal Code (OM)
                                   Minimum SERP Rural Standards

Language Options:                  Automatic

Valid:                                              8246
Corrected Addresses:                                   3
Uncorrectable Addresses:                               0
Foreign Addresses:                                     0

LVR Questionable:                                      7
Rural Questionable and Valid:                        138
Rural Questionable and Corrected:                      2



Questionable Rural Addresses:                       1.7%


Percent Valid Before Correction:                  100.0%
Percent Valid:                                    100.0%


Total Records:                                      8249

Date file processed:                             2017-12-22 12:42
Address Accuracy Expiry Date:                    2018-12-22
