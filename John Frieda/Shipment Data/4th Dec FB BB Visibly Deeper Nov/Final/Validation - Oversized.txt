Job Name:                                        FB BB VD OS 5609

Software & Version Number:                       iAddress Version 2014

Customer Name:                                   Samplits
Customer Address:                                5595 Finch Ave E
                                                 Toronto ON  M1B2T9
Customer Telephone Number:                       (416) 572-5222
Customer Number:                                 1018221
Mailed on Behalf Of:                             Samplits Inc
Customer Number:                                 1018221
Date of Mailing Selected:                        2017-12-05

Expiry Date of Delivery Mode Data CD:            2017-12-14
Serial Number of Delivery Mode Data CD:          P3102025561101

Options:                           Upper Case
                                   Two Line
                                   Western Style
                                   Shorten Streets
                                   Update Valid
                                   Place Space in Postal Code (OM)
                                   Minimum SERP Rural Standards

Language Options:                  Automatic

Valid:                                              5326
Corrected Addresses:                                 127
Uncorrectable Addresses:                               0
Foreign Addresses:                                     0

LVR Questionable:                                      4
Rural Questionable and Valid:                        121
Rural Questionable and Corrected:                      4



Questionable Rural Addresses:                       2.3%


Percent Valid Before Correction:                   97.7%
Percent Valid:                                    100.0%


Total Records:                                      5453

Date file processed:                             2017-12-04 14:13
Address Accuracy Expiry Date:                    2018-12-04
