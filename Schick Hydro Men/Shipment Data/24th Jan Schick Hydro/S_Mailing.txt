               Statement of Mailing               


Job Name: Schick Hydro unshipped orders-4845
Input File: C:\Users\User\Documents\Samplits - PPP Corp\samplits1\Schick Hydro Men\Shipment Data\24th Jan Schick Hydro\Schick Hydro unshipped orders-4845.xlsx

Customer Name                           : Samplits
Customer Address                        : 5595 Finch Ave E
Customer Telephone Number               : (416) 572-5222
Customer Number                         : 1018221
Mailed on Behalf Of                     : Samplits Inc
Customer Number                         : 1018221

Software & Version Number               : iAddress  - Version 4 SERP Recognized
SERP Expiry Date                        : 2019-01-10
Database Valid From                     : 2018-01-12 To 2018-02-15
Delivery Mode Audit Code                : H
Serial Number of Delivery Mode Data CD  : P3102025561101
Office of Deposit                       : MISSISSAUGA ON
Date of Mailing                         : 2018-01-26 00:00:00
Run Date of Mailing Summary             : 2018-01-24 14:30:03

Mailing Plan ID                         : EST_E503644513
Mailing Type                            : Personalized Mail SH
Piece Type                              : OVERSIZED
Container Type                          : Flattub
Monotainer/Pallet                       : Pallet
Maximum Letters per Container           : 17
Maximum Letters per Bundle              : 9(Bundle Split)
Piece Length                            : 215.90 mm / 8.50 In
Piece Height                            : 0.00 mm / 0.00 In
Piece Thickness                         : 29.44 mm / 1.16 In
Piece Weight                            : 48.00 g / 1.69 oz
Total number of Bundles                 : 902
Total number of Containers              : 346
Total number of Monotainers/Pallets     : 15
Total Weight                            : 217.5360 KG / 479.5850 lbs

Mailing Summary                     Pieces                  Price        Total Cost
    Special Handling           :      4532               $0.67000         $3,036.44
                                    ------                               ----------
                       SUB-TOTAL      4532                                $3,036.44
                             TAX                              13%           $394.74
                                                                         ----------
                           TOTAL                                          $3,431.18

IMPORTANT:
The amounts listed on this Statement of Mailing are based on the most
updated information provided by Canada Post. Final totals, including
applicable taxes, will be calculated by Canada Post's Electronic Shipping Tools.



